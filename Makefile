.DEFAULT_GOAL := help

.PHONY: help clean

# Variables
IMAGE_NAME ?= quay.io/testing-farm/worker-public
# default image tag set to current user name
IMAGE_TAG ?= ${USER}
GM_TAG ?= ${IMAGE_TAG}

# Help prelude
define PRELUDE

Usage:
  make [target]

endef

# Help targets
init:
	@mkdir -p $(DEVELOPMENT_DATA)

##@ Containers

container/build:  ## Build containers
	buildah bud --build-arg TAG=$(GM_TAG) -t $(IMAGE_NAME):$(IMAGE_TAG) -f container/Containerfile .

container/push:  ## Push containers
	buildah push $(IMAGE_NAME):$(IMAGE_TAG)

##@ Utility

# See https://www.thapaliya.com/en/writings/well-documented-makefiles/ for details.
reverse = $(if $(1),$(call reverse,$(wordlist 2,$(words $(1)),$(1)))) $(firstword $(1))

help:  ## Show this help
	@awk 'BEGIN {FS = ":.*##"; printf "$(info $(PRELUDE))"} /^[a-zA-Z_/-]+:.*?##/ { printf "  \033[36m%-35s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(call reverse, $(MAKEFILE_LIST))
